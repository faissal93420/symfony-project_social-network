<?php

// pour créer cette page on pourrait faire aussi un : symfony console make:controller, puis son nom

namespace App\Controller;


use App\Entity\Post;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IntroController extends AbstractController
{
//    /**
//     * @Route("/homepage", name="app_homepage")
//     * @param Request $request
//     * @return Response
//     */
//    public function hello(Request $request): Response
//    {
////        dump($request);
////        exit;
//        // On réccup le nom depuis le param de l'URL (GET)
//        $name = $request->query->get('name');
//
//        /**
//         * @var PostRepository $repo
//         */
//        $repo = $this->getDoctrine()->getRepository(Post::class);
//        $recentPosts = $repo->findRecentPosts(3);
//
//        return $this->render("homepage.html.twig", [
//           "name" => $name,
//            "recentPosts"=>$recentPosts
//        ]);
//    }

//    /**
//     * @Route ("/gowiki")
//     * @return RedirectResponse
//     */
//    public function gowiki(): Response
//    {
//        return new RedirectResponse("https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal");
//    }


    /**
     * @Route ("/about_us", name="app_about")
     * @return Response
     */
    public function goAbout(): Response
    {
        return $this->render("about.html.twig");
    }
}