<?php


namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

// implements DependentFixtureInterface sert à ce que ce fichier (postfixtures) soit chargé après le "UserFixtures",
// associé à la deuxième fonction getDépendencies
class PostFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager){

        $faker = Factory::create("fr_FR");

        // on reccup un utilisateur avec l'objet manager plus haut
        $users = $manager->getRepository(User::class)->findAll();

        for ($i= 1; $i<=100 ; $i++){
            // on sélectionne un utilisateur aléatoirement
            $randomIndex = array_rand($users);
            /** @var User $author */
            $author = $users[$randomIndex];

            $post = new Post();

            // On définit un nombre de phrase entre 1 et 5, ainsi que 280 car max (on tronque la chaine)
            $content = $faker->sentences(rand(1, 5), true);
            $content = substr($content, 0 , 280);

            $post->setAuthor($author);
            $post->setContent($content);
            $post->setCreatedAt($faker->dateTimeThisYear);

            $manager->persist($post);

            $randomComment = rand(0, 12);
            for ($j=1; $j<$randomComment ; $j++){
                $comment = new Post();

                $contentComment = $faker->sentences(rand(1, 5), true);
                $contentComment = substr($contentComment, 0, 280);
                $comment->setAuthor($users[array_rand($users)]);
                $comment->setContent($contentComment);
                $comment->setCreatedAt($faker->dateTimeThisYear);

                $comment->setParent($post);
                // on peut aussi faire l'inverse
                // $post->addComment($comment)

                $manager->persist($comment);
            }
        }

        $manager->flush();

    }

    public function getDependencies()
    {
        return  [
            UserFixtures::class
        ];
    }
}