<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;


    /**
     * UserFixtures constructor.
     */
    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create("fr_FR");
        $gender = rand(1, 100) <= 50 ? 'male' : 'female';

        $nbUser = rand(15, 20);
        for ($i=0 ; $i<$nbUser ;$i++){
            $user = new User();
            $user->setUsername($faker->userName);
            $user->setBirthday($faker->dateTimeBetween('-80 years', '-18 years'));
            $user->setCreatedAt(new \DateTime());
            $user->setEmail($faker->email);
            $user->setFirstName($faker->firstName($gender));
            $user->setLastName($faker->lastName);
            $user->setGender($gender);
            $user->setTelephone($faker->phoneNumber);

            $realPassword = "azerty";
            $hash = $this->userPasswordEncoder->encodePassword($user, $realPassword);
            $user->setPassword("$hash");


            $manager->persist($user);
        }

        $manager->flush();
    }
}
