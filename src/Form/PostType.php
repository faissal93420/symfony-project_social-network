<?php

namespace App\Form;

use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', null, ['label'=>'Ecrivez ici !'])
            ->add('submit', SubmitType::class, ['label'=>'Envoyer'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        // a chaque envoie de form, on creer un objet Post
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
