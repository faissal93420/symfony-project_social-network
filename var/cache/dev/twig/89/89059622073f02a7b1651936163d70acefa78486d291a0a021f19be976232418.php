<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /userprofil.html.twig */
class __TwigTemplate_bc1fec88cfe780f1b1a2f178a3553ad5470b8676af269f806adbc767e46ef445 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/userprofil.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/userprofil.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "/userprofil.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    Profil utilisateur
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    ";
        $this->loadTemplate("menu.html.twig", "/userprofil.html.twig", 8)->display($context);
        // line 9
        echo "
    <div class=\"container-sm\">
        <br>
        <br>

        ";
        // line 14
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 14, $this->source); })()), "user", [], "any", false, false, false, 14), "username", [], "any", false, false, false, 14), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 14, $this->source); })())))) {
            // line 15
            echo "
            <h2>Mon profil</h2>
            <br>
            <h4>Tous mes post</h4>
        ";
        } else {
            // line 20
            echo "            <div class=\"d-flex\">
                <h2>Profil de ";
            // line 21
            echo twig_escape_filter($this->env, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 21, $this->source); })()), "html", null, true);
            echo "</h2>
            </div>
        ";
        }
        // line 24
        echo "


        <br>
        <br>


        <div class=\"container-sm d-flex\">
            <ul class=\"list-group list-unstyled col-6\">

                ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["parents"]) || array_key_exists("parents", $context) ? $context["parents"] : (function () { throw new RuntimeError('Variable "parents" does not exist.', 34, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 35
            echo "                        <li class=\"mb-3\">
                            <div class=\"card\">
                                <div class=\"card-header\">
                                    ";
            // line 38
            $this->loadTemplate("avatar.html.twig", "/userprofil.html.twig", 38)->display(twig_array_merge($context, ["username" => twig_get_attribute($this->env, $this->source, $context["post"], "author", [], "any", false, false, false, 38)]));
            // line 39
            echo "                                    #";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "id", [], "any", false, false, false, 39), "html", null, true);
            echo " by <strong><a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("userprofil", ["user" => twig_get_attribute($this->env, $this->source, $context["post"], "author", [], "any", false, false, false, 39)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "author", [], "any", false, false, false, 39), "html", null, true);
            echo "</a></strong>
                                </div>
                                <div class=\"card-body\">
                                    <p class=\"card-text\">
                                        ";
            // line 43
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "content", [], "any", false, false, false, 43), "html", null, true);
            echo "
                                    </p>
                                </div>
                            </div>
                        </li>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "            </ul>
            <div class=\"colVide col-1\"></div>
            <div class=\"nbrPost col-2\">
                ";
        // line 52
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 52, $this->source); })()), "user", [], "any", false, false, false, 52), "username", [], "any", false, false, false, 52), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 52, $this->source); })())))) {
            // line 53
            echo "                ";
        } else {
            // line 54
            echo "                    <div class=\"d-flex align-items-center mb-3\">
                        <div>
                            ";
            // line 56
            if (twig_get_attribute($this->env, $this->source, (isset($context["getFollowers"]) || array_key_exists("getFollowers", $context) ? $context["getFollowers"] : (function () { throw new RuntimeError('Variable "getFollowers" does not exist.', 56, $this->source); })()), "contains", [0 => twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 56, $this->source); })()), "user", [], "any", false, false, false, 56)], "method", false, false, false, 56)) {
                // line 57
                echo "                                <a class=\"btn-sm btn-danger fas fa-minus-square\" href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_follow", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 57, $this->source); })()), "id", [], "any", false, false, false, 57), "user" => (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 57, $this->source); })())]), "html", null, true);
                echo "\"></a>
                            ";
            } else {
                // line 59
                echo "                                <a class=\"btn-sm btn-primary fas fa-plus-square d-flex align-items-center\" href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_follow", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 59, $this->source); })()), "id", [], "any", false, false, false, 59), "user" => (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 59, $this->source); })())]), "html", null, true);
                echo "\"></a>
                            ";
            }
            // line 61
            echo "                        </div>
                        <div>
                            ";
            // line 63
            if (twig_get_attribute($this->env, $this->source, (isset($context["getFriends"]) || array_key_exists("getFriends", $context) ? $context["getFriends"] : (function () { throw new RuntimeError('Variable "getFriends" does not exist.', 63, $this->source); })()), "contains", [0 => twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 63, $this->source); })()), "user", [], "any", false, false, false, 63)], "method", false, false, false, 63)) {
                // line 64
                echo "                                ";
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 64, $this->source); })()), "user", [], "any", false, false, false, 64), "username", [], "any", false, false, false, 64), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 64, $this->source); })())))) {
                    // line 65
                    echo "                                    <a class=\"btn-sm btn-success ms-3\" href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_friends", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 65, $this->source); })()), "id", [], "any", false, false, false, 65), "user" => (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 65, $this->source); })())]), "html", null, true);
                    echo "\">Accepter demande d'ami</a>
                                ";
                } else {
                    // line 67
                    echo "                                    <a class=\"btn-sm btn-warning ms-3\" href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_friends", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 67, $this->source); })()), "id", [], "any", false, false, false, 67), "user" => (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 67, $this->source); })())]), "html", null, true);
                    echo "\">Demande envoyée</a>
                                ";
                }
                // line 69
                echo "                            ";
            } else {
                // line 70
                echo "                                <a class=\"fas fa-user-plus btn-sm btn-warning ms-3\" href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_friends", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 70, $this->source); })()), "id", [], "any", false, false, false, 70), "user" => (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 70, $this->source); })())]), "html", null, true);
                echo "\"></a>
                            ";
            }
            // line 72
            echo "                        </div>
                    </div>
                ";
        }
        // line 75
        echo "
                ";
        // line 76
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 76, $this->source); })()), "user", [], "any", false, false, false, 76), "username", [], "any", false, false, false, 76), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 76, $this->source); })())))) {
            // line 77
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["getFriends"]) || array_key_exists("getFriends", $context) ? $context["getFriends"] : (function () { throw new RuntimeError('Variable "getFriends" does not exist.', 77, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["friends"]) {
                // line 78
                echo "                        <div class=\"d-flex\">
                            <a class=\"btn btn-warning\" href=\"";
                // line 79
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_friends", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 79, $this->source); })()), "id", [], "any", false, false, false, 79), "user" => twig_get_attribute($this->env, $this->source, $context["friends"], "username", [], "any", false, false, false, 79)]), "html", null, true);
                echo "\">Accepter la demande de</a>
                            <li class=\"text-primary list-unstyled\">";
                // line 80
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["friends"], "username", [], "any", false, false, false, 80), "html", null, true);
                echo "</li>
                        </div>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['friends'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 83
            echo "                ";
        }
        // line 84
        echo "

                ";
        // line 86
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["peopleIaccept"]) || array_key_exists("peopleIaccept", $context) ? $context["peopleIaccept"] : (function () { throw new RuntimeError('Variable "peopleIaccept" does not exist.', 86, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["friends"]) {
            // line 87
            echo "                    <li class=\"text-danger\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["friends"], "username", [], "any", false, false, false, 87), "html", null, true);
            echo "</li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['friends'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "                <div class=\"accordion accordion-flush card\" id=\"accordionFlushExample\">
                    <div class=\"accordion-item-header\">
                        <h2 class=\"accordion-header\" id=\"flush-headingOne\">
                            <button class=\"accordion-button collapsed accordion-button-primary text-primary\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#flush-collapseOne\" aria-expanded=\"false\" aria-controls=\"flush-collapseOne\">
                                Followers | ";
        // line 93
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["getFollowers"]) || array_key_exists("getFollowers", $context) ? $context["getFollowers"] : (function () { throw new RuntimeError('Variable "getFollowers" does not exist.', 93, $this->source); })())), "html", null, true);
        echo "
                            </button>
                        </h2>
                        <div id=\"flush-collapseOne\" class=\"accordion-collapse collapse\" aria-labelledby=\"flush-headingOne\" data-bs-parent=\"#accordionFlushExample\">
                            <div class=\"accordion-body\">
                                <ul>
                                    ";
        // line 99
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["getFollowers"]) || array_key_exists("getFollowers", $context) ? $context["getFollowers"] : (function () { throw new RuntimeError('Variable "getFollowers" does not exist.', 99, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["followers"]) {
            // line 100
            echo "                                        <li>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["followers"], "username", [], "any", false, false, false, 100), "html", null, true);
            echo "</li>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['followers'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 102
        echo "                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class=\"accordion accordion-flush card\" id=\"accordionFlushExample\">
                    <div class=\"accordion-item-header\">
                        <h2 class=\"accordion-header\" id=\"flush-headingOne\">
                            <button class=\"accordion-button collapsed accordion-button-primary text-primary\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#flush-collapseTwo\" aria-expanded=\"false\" aria-controls=\"flush-collapseOne\">
                                People I follow | ";
        // line 112
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["peopleIFollow"]) || array_key_exists("peopleIFollow", $context) ? $context["peopleIFollow"] : (function () { throw new RuntimeError('Variable "peopleIFollow" does not exist.', 112, $this->source); })())), "html", null, true);
        echo "
                            </button>
                        </h2>
                        <div id=\"flush-collapseTwo\" class=\"accordion-collapse collapse\" aria-labelledby=\"flush-headingOne\" data-bs-parent=\"#accordionFlushExample\">
                            <div class=\"accordion-body\">
                                <ul>
                                    ";
        // line 118
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["peopleIFollow"]) || array_key_exists("peopleIFollow", $context) ? $context["peopleIFollow"] : (function () { throw new RuntimeError('Variable "peopleIFollow" does not exist.', 118, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["followers"]) {
            // line 119
            echo "                                        <li>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["followers"], "username", [], "any", false, false, false, 119), "html", null, true);
            echo "</li>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['followers'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 121
        echo "                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class=\"card\">
                    <h5 class=\"card-header\">Amis</h5>
                    <div class=\"card-body\">6</div>
                </div>
                <br>
                <div class=\"card\">
                    <h5 class=\"card-header\">Nombre de posts</h5>
                    <div class=\"card-body\">";
        // line 134
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["parents"]) || array_key_exists("parents", $context) ? $context["parents"] : (function () { throw new RuntimeError('Variable "parents" does not exist.', 134, $this->source); })())), "html", null, true);
        echo "</div>
                </div>
            </div>
        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "/userprofil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  372 => 134,  357 => 121,  348 => 119,  344 => 118,  335 => 112,  323 => 102,  314 => 100,  310 => 99,  301 => 93,  295 => 89,  286 => 87,  282 => 86,  278 => 84,  275 => 83,  266 => 80,  262 => 79,  259 => 78,  254 => 77,  252 => 76,  249 => 75,  244 => 72,  238 => 70,  235 => 69,  229 => 67,  223 => 65,  220 => 64,  218 => 63,  214 => 61,  208 => 59,  202 => 57,  200 => 56,  196 => 54,  193 => 53,  191 => 52,  186 => 49,  166 => 43,  154 => 39,  152 => 38,  147 => 35,  130 => 34,  118 => 24,  112 => 21,  109 => 20,  102 => 15,  100 => 14,  93 => 9,  90 => 8,  80 => 7,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}

{% block title %}
    Profil utilisateur
{% endblock %}

{% block body %}
    {% include \"menu.html.twig\" %}

    <div class=\"container-sm\">
        <br>
        <br>

        {% if app.user.username == user %}

            <h2>Mon profil</h2>
            <br>
            <h4>Tous mes post</h4>
        {% else %}
            <div class=\"d-flex\">
                <h2>Profil de {{ user }}</h2>
            </div>
        {% endif %}



        <br>
        <br>


        <div class=\"container-sm d-flex\">
            <ul class=\"list-group list-unstyled col-6\">

                {% for post in parents %}
                        <li class=\"mb-3\">
                            <div class=\"card\">
                                <div class=\"card-header\">
                                    {% include \"avatar.html.twig\" with { username: post.author} %}
                                    #{{ post.id }} by <strong><a href=\"{{ path('userprofil', {user:post.author}) }}\">{{ post.author }}</a></strong>
                                </div>
                                <div class=\"card-body\">
                                    <p class=\"card-text\">
                                        {{ post.content }}
                                    </p>
                                </div>
                            </div>
                        </li>
                {% endfor %}
            </ul>
            <div class=\"colVide col-1\"></div>
            <div class=\"nbrPost col-2\">
                {% if app.user.username == user %}
                {% else %}
                    <div class=\"d-flex align-items-center mb-3\">
                        <div>
                            {% if getFollowers.contains(app.user) %}
                                <a class=\"btn-sm btn-danger fas fa-minus-square\" href=\"{{ path('app_follow', {id:user.id, user:user}) }}\"></a>
                            {% else %}
                                <a class=\"btn-sm btn-primary fas fa-plus-square d-flex align-items-center\" href=\"{{ path('app_follow', {id:user.id, user:user}) }}\"></a>
                            {% endif %}
                        </div>
                        <div>
                            {% if getFriends.contains(app.user) %}
                                {% if app.user.username == user %}
                                    <a class=\"btn-sm btn-success ms-3\" href=\"{{ path('app_friends', {id: user.id, user:user}) }}\">Accepter demande d'ami</a>
                                {% else %}
                                    <a class=\"btn-sm btn-warning ms-3\" href=\"{{ path('app_friends', {id: user.id, user:user}) }}\">Demande envoyée</a>
                                {% endif %}
                            {% else %}
                                <a class=\"fas fa-user-plus btn-sm btn-warning ms-3\" href=\"{{ path('app_friends', {id: user.id, user:user}) }}\"></a>
                            {% endif %}
                        </div>
                    </div>
                {% endif %}

                {% if app.user.username == user %}
                    {% for friends in getFriends %}
                        <div class=\"d-flex\">
                            <a class=\"btn btn-warning\" href=\"{{ path('app_friends', {id: user.id, user:friends.username}) }}\">Accepter la demande de</a>
                            <li class=\"text-primary list-unstyled\">{{ friends.username }}</li>
                        </div>
                    {% endfor %}
                {% endif %}


                {% for friends in peopleIaccept %}
                    <li class=\"text-danger\">{{ friends.username }}</li>
                {% endfor %}
                <div class=\"accordion accordion-flush card\" id=\"accordionFlushExample\">
                    <div class=\"accordion-item-header\">
                        <h2 class=\"accordion-header\" id=\"flush-headingOne\">
                            <button class=\"accordion-button collapsed accordion-button-primary text-primary\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#flush-collapseOne\" aria-expanded=\"false\" aria-controls=\"flush-collapseOne\">
                                Followers | {{ getFollowers | length }}
                            </button>
                        </h2>
                        <div id=\"flush-collapseOne\" class=\"accordion-collapse collapse\" aria-labelledby=\"flush-headingOne\" data-bs-parent=\"#accordionFlushExample\">
                            <div class=\"accordion-body\">
                                <ul>
                                    {% for followers in getFollowers %}
                                        <li>{{ followers.username }}</li>
                                    {% endfor %}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class=\"accordion accordion-flush card\" id=\"accordionFlushExample\">
                    <div class=\"accordion-item-header\">
                        <h2 class=\"accordion-header\" id=\"flush-headingOne\">
                            <button class=\"accordion-button collapsed accordion-button-primary text-primary\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#flush-collapseTwo\" aria-expanded=\"false\" aria-controls=\"flush-collapseOne\">
                                People I follow | {{ peopleIFollow | length }}
                            </button>
                        </h2>
                        <div id=\"flush-collapseTwo\" class=\"accordion-collapse collapse\" aria-labelledby=\"flush-headingOne\" data-bs-parent=\"#accordionFlushExample\">
                            <div class=\"accordion-body\">
                                <ul>
                                    {% for followers in peopleIFollow %}
                                        <li>{{ followers.username }}</li>
                                    {% endfor %}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class=\"card\">
                    <h5 class=\"card-header\">Amis</h5>
                    <div class=\"card-body\">6</div>
                </div>
                <br>
                <div class=\"card\">
                    <h5 class=\"card-header\">Nombre de posts</h5>
                    <div class=\"card-body\">{{ parents | length }}</div>
                </div>
            </div>
        </div>
    </div>

{% endblock %}
", "/userprofil.html.twig", "/home/clem/INFREP/introduction-symfo/templates/userprofil.html.twig");
    }
}
