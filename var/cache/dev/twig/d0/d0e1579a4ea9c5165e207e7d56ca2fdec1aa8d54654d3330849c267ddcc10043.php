<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /comment.html.twig */
class __TwigTemplate_d1dc4faa8e59716c80a14afe6bd58535cb5c4624fbe19d323aa31b43b675537d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/comment.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "/comment.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "/comment.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    Commentaires du post ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 4, $this->source); })()), "id", [], "any", false, false, false, 4), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "    ";
        $this->loadTemplate("menu.html.twig", "/comment.html.twig", 8)->display($context);
        // line 9
        echo "
    <br>
    <br>
<div class=\"container-sm\">
    <div class=\"container d-flex\">
        <div class=\"container col-7\">
            <h3>Commentaires du Post # ";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 15, $this->source); })()), "id", [], "any", false, false, false, 15), "html", null, true);
        echo "</h3>

            <div class=\" mb-3\">
                <div class=\"card\">
                    <div class=\"card-header\">
                        ";
        // line 20
        $this->loadTemplate("avatar.html.twig", "/comment.html.twig", 20)->display(twig_array_merge($context, ["username" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 20, $this->source); })()), "author", [], "any", false, false, false, 20)]));
        // line 21
        echo "                        #";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 21, $this->source); })()), "id", [], "any", false, false, false, 21), "html", null, true);
        echo " by <strong><a href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("userprofil", ["user" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 21, $this->source); })()), "author", [], "any", false, false, false, 21)]), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 21, $this->source); })()), "author", [], "any", false, false, false, 21), "html", null, true);
        echo "</a></strong>
                    </div>
                    <div class=\"card-body\">
                        <div class=\"card-text\">
                            ";
        // line 25
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 25, $this->source); })()), "content", [], "any", false, false, false, 25), "html", null, true);
        echo "
                        </div>
                        <div class=\"container d-flex justify-content-end\">
                            <a href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_comment", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 28, $this->source); })()), "id", [], "any", false, false, false, 28)]), "html", null, true);
        echo "\">Voir les commentaires (";
        echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 28, $this->source); })()), "comment", [], "any", false, false, false, 28)), "html", null, true);
        echo ")
                            </a>
                            ";
        // line 30
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 30, $this->source); })()), "user", [], "any", false, false, false, 30), "username", [], "any", false, false, false, 30), twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 30, $this->source); })()), "author", [], "any", false, false, false, 30)))) {
            // line 31
            echo "                                <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("formulaire-update", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 31, $this->source); })()), "id", [], "any", false, false, false, 31)]), "html", null, true);
            echo "\" class=\"btn btn-warning btn-sm fas fa-pen \"></a>
                            ";
        }
        // line 33
        echo "                            <a class=\"btn btn-danger btn-sm fas fa-heart ms-2\"></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"container d-flex comment\">
                <div class=\"col-1\">
                </div>
                <div class=\"mb-3\">
                    ";
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 43, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 44
            echo "                        <div class=\"card mb-3\">
                            <div class=\"card-header\">
                                ";
            // line 46
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comment"], "id", [], "any", false, false, false, 46), "html", null, true);
            echo "

                                ";
            // line 48
            $this->loadTemplate("avatar.html.twig", "/comment.html.twig", 48)->display(twig_array_merge($context, ["username" => twig_get_attribute($this->env, $this->source, $context["comment"], "author", [], "any", false, false, false, 48)]));
            // line 49
            echo "                                #";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comment"], "id", [], "any", false, false, false, 49), "html", null, true);
            echo " by <strong><a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("userprofil", ["user" => twig_get_attribute($this->env, $this->source, $context["comment"], "author", [], "any", false, false, false, 49)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comment"], "author", [], "any", false, false, false, 49), "html", null, true);
            echo "</a></strong>
                            </div>
                            <div class=\"card-body\">
                                <div class=\"card-text mb-2\">
                                    ";
            // line 53
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comment"], "content", [], "any", false, false, false, 53), "html", null, true);
            echo "
                                </div>
                                <div class=\"container d-flex justify-content-end\">
                                    ";
            // line 57
            echo "                                    <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_comment", ["id" => twig_get_attribute($this->env, $this->source, $context["comment"], "id", [], "any", false, false, false, 57)]), "html", null, true);
            echo "\">Voir les commentaires (";
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comment"], "comment", [], "any", false, false, false, 57)), "html", null, true);
            echo ")</a>
                                    ";
            // line 58
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 58, $this->source); })()), "user", [], "any", false, false, false, 58), "username", [], "any", false, false, false, 58), twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 58, $this->source); })()), "author", [], "any", false, false, false, 58)))) {
                // line 59
                echo "                                        <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("formulaire-update", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["post"]) || array_key_exists("post", $context) ? $context["post"] : (function () { throw new RuntimeError('Variable "post" does not exist.', 59, $this->source); })()), "id", [], "any", false, false, false, 59)]), "html", null, true);
                echo "\" class=\"btn btn-warning btn-sm fas fa-pen \"></a>
                                    ";
            }
            // line 61
            echo "                                    <a class=\"btn btn-danger btn-sm fas fa-heart ms-2\"></a>
                                </div>
                            </div>
                        </div>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "                </div>
            </div>
        </div>
        <div class=\"container col-5\">
            <div class=\"card>
                <div class=\"card-header\">
                    <h3>Post ton commentaire !</h3>
                </div>
                <div class=\"card-header\">
                    ";
        // line 75
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formComment"]) || array_key_exists("formComment", $context) ? $context["formComment"] : (function () { throw new RuntimeError('Variable "formComment" does not exist.', 75, $this->source); })()), 'form');
        echo "
                </div>
            </div>
        </div>
    </div>


</div>




";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "/comment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  248 => 75,  237 => 66,  219 => 61,  213 => 59,  211 => 58,  204 => 57,  198 => 53,  186 => 49,  184 => 48,  179 => 46,  175 => 44,  158 => 43,  146 => 33,  140 => 31,  138 => 30,  131 => 28,  125 => 25,  113 => 21,  111 => 20,  103 => 15,  95 => 9,  92 => 8,  82 => 7,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}
    Commentaires du post {{ post.id }}
{% endblock %}

{% block body %}
    {% include \"menu.html.twig\" %}

    <br>
    <br>
<div class=\"container-sm\">
    <div class=\"container d-flex\">
        <div class=\"container col-7\">
            <h3>Commentaires du Post # {{ post.id }}</h3>

            <div class=\" mb-3\">
                <div class=\"card\">
                    <div class=\"card-header\">
                        {% include \"avatar.html.twig\" with { username: post.author} %}
                        #{{ post.id }} by <strong><a href=\"{{ path('userprofil', {user:post.author}) }}\">{{ post.author }}</a></strong>
                    </div>
                    <div class=\"card-body\">
                        <div class=\"card-text\">
                            {{ post.content }}
                        </div>
                        <div class=\"container d-flex justify-content-end\">
                            <a href=\"{{ path('app_comment', {id:post.id}) }}\">Voir les commentaires ({{ post.comment | length }})
                            </a>
                            {% if app.user.username == post.author %}
                                <a href=\"{{ path('formulaire-update', {id:post.id}) }}\" class=\"btn btn-warning btn-sm fas fa-pen \"></a>
                            {% endif %}
                            <a class=\"btn btn-danger btn-sm fas fa-heart ms-2\"></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"container d-flex comment\">
                <div class=\"col-1\">
                </div>
                <div class=\"mb-3\">
                    {% for comment in comments %}
                        <div class=\"card mb-3\">
                            <div class=\"card-header\">
                                {{ comment.id }}

                                {% include \"avatar.html.twig\" with { username: comment.author} %}
                                #{{ comment.id }} by <strong><a href=\"{{ path('userprofil', {user:comment.author}) }}\">{{ comment.author }}</a></strong>
                            </div>
                            <div class=\"card-body\">
                                <div class=\"card-text mb-2\">
                                    {{ comment.content }}
                                </div>
                                <div class=\"container d-flex justify-content-end\">
                                    {# VERIFIER QUE TOUT SOIT BON EN DESSOUS #}
                                    <a href=\"{{ path('app_comment', {id:comment.id}) }}\">Voir les commentaires ({{ comment.comment | length }})</a>
                                    {% if app.user.username == post.author %}
                                        <a href=\"{{ path('formulaire-update', {id:post.id}) }}\" class=\"btn btn-warning btn-sm fas fa-pen \"></a>
                                    {% endif %}
                                    <a class=\"btn btn-danger btn-sm fas fa-heart ms-2\"></a>
                                </div>
                            </div>
                        </div>
                    {% endfor %}
                </div>
            </div>
        </div>
        <div class=\"container col-5\">
            <div class=\"card>
                <div class=\"card-header\">
                    <h3>Post ton commentaire !</h3>
                </div>
                <div class=\"card-header\">
                    {{ form(formComment) }}
                </div>
            </div>
        </div>
    </div>


</div>




{% endblock %}", "/comment.html.twig", "/home/clem/INFREP/introduction-symfo/templates/comment.html.twig");
    }
}
