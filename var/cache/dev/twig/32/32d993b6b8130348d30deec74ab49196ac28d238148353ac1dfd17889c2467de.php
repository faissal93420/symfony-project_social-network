<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* registration/registration-update.html.twig */
class __TwigTemplate_d49c1f67870559c45fee6d4c3e2cf4be21ce04487076af12f9970e13d96be988 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "registration/registration-update.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "registration/registration-update.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "registration/registration-update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Modifier vos informations";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    ";
        $this->loadTemplate("menu.html.twig", "registration/registration-update.html.twig", 6)->display($context);
        // line 7
        echo "
    ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 8, $this->source); })()), "flashes", [0 => "verify_email_error"], "method", false, false, false, 8));
        foreach ($context['_seq'] as $context["_key"] => $context["flashError"]) {
            // line 9
            echo "        <div class=\"alert alert-danger\" role=\"alert\">";
            echo twig_escape_filter($this->env, $context["flashError"], "html", null, true);
            echo "</div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashError'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "
    <div class=\"container-sm d-flex justify-content-center\">
        <div class=\"col-6 ml-10\">
            <br>
            <br>
            <br>
            <div class=\"card\">
                <div class=\"card-header d-flex justify-content-between\">
                    <div class=\"bloc\">
                        <h1>Modifier vos informations</h1>
                    </div>

                </div>
                <div class=\"card-body\">
                    ";
        // line 25
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["updateRegistration"]) || array_key_exists("updateRegistration", $context) ? $context["updateRegistration"] : (function () { throw new RuntimeError('Variable "updateRegistration" does not exist.', 25, $this->source); })()), 'form_start');
        echo "
                    ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["updateRegistration"]) || array_key_exists("updateRegistration", $context) ? $context["updateRegistration"] : (function () { throw new RuntimeError('Variable "updateRegistration" does not exist.', 26, $this->source); })()), "username", [], "any", false, false, false, 26), 'row', ["label" => "Pseudo"]);
        echo "
                    ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["updateRegistration"]) || array_key_exists("updateRegistration", $context) ? $context["updateRegistration"] : (function () { throw new RuntimeError('Variable "updateRegistration" does not exist.', 27, $this->source); })()), "email", [], "any", false, false, false, 27), 'row', ["label" => "Email"]);
        echo "
                    ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["updateRegistration"]) || array_key_exists("updateRegistration", $context) ? $context["updateRegistration"] : (function () { throw new RuntimeError('Variable "updateRegistration" does not exist.', 28, $this->source); })()), "first_name", [], "any", false, false, false, 28), 'row', ["label" => "Prénom"]);
        echo "
                    ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["updateRegistration"]) || array_key_exists("updateRegistration", $context) ? $context["updateRegistration"] : (function () { throw new RuntimeError('Variable "updateRegistration" does not exist.', 29, $this->source); })()), "last_name", [], "any", false, false, false, 29), 'row', ["label" => "Nom"]);
        echo "
                    ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["updateRegistration"]) || array_key_exists("updateRegistration", $context) ? $context["updateRegistration"] : (function () { throw new RuntimeError('Variable "updateRegistration" does not exist.', 30, $this->source); })()), "birthday", [], "any", false, false, false, 30), 'row', ["label" => "Anniversaire"]);
        echo "
                    ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["updateRegistration"]) || array_key_exists("updateRegistration", $context) ? $context["updateRegistration"] : (function () { throw new RuntimeError('Variable "updateRegistration" does not exist.', 31, $this->source); })()), "gender", [], "any", false, false, false, 31), 'row', ["label" => "Genre"]);
        echo "
                    ";
        // line 32
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["updateRegistration"]) || array_key_exists("updateRegistration", $context) ? $context["updateRegistration"] : (function () { throw new RuntimeError('Variable "updateRegistration" does not exist.', 32, $this->source); })()), "telephone", [], "any", false, false, false, 32), 'row', ["label" => "Téléphone"]);
        echo "
                    <div class=\"bloc d-flex justify-content-end\">
                        <button type=\"submit\" class=\"btn btn-primary mt-3\">Valider</button>
                    </div>

                    ";
        // line 37
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["updateRegistration"]) || array_key_exists("updateRegistration", $context) ? $context["updateRegistration"] : (function () { throw new RuntimeError('Variable "updateRegistration" does not exist.', 37, $this->source); })()), 'form_end');
        echo "
                </div>
            </div>

            <br>
            <br>



        </div>
    </div>



";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "registration/registration-update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 37,  151 => 32,  147 => 31,  143 => 30,  139 => 29,  135 => 28,  131 => 27,  127 => 26,  123 => 25,  107 => 11,  98 => 9,  94 => 8,  91 => 7,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Modifier vos informations{% endblock %}

{% block body %}
    {% include \"menu.html.twig\" %}

    {% for flashError in app.flashes('verify_email_error') %}
        <div class=\"alert alert-danger\" role=\"alert\">{{ flashError }}</div>
    {% endfor %}

    <div class=\"container-sm d-flex justify-content-center\">
        <div class=\"col-6 ml-10\">
            <br>
            <br>
            <br>
            <div class=\"card\">
                <div class=\"card-header d-flex justify-content-between\">
                    <div class=\"bloc\">
                        <h1>Modifier vos informations</h1>
                    </div>

                </div>
                <div class=\"card-body\">
                    {{ form_start(updateRegistration) }}
                    {{ form_row(updateRegistration.username, { label : 'Pseudo'}) }}
                    {{ form_row(updateRegistration.email, {label: 'Email' }) }}
                    {{ form_row(updateRegistration.first_name, { label: 'Prénom' }) }}
                    {{ form_row(updateRegistration.last_name, { label: 'Nom' }) }}
                    {{ form_row(updateRegistration.birthday, { label: 'Anniversaire' }) }}
                    {{ form_row(updateRegistration.gender, { label: 'Genre' }) }}
                    {{ form_row(updateRegistration.telephone, { label: 'Téléphone' }) }}
                    <div class=\"bloc d-flex justify-content-end\">
                        <button type=\"submit\" class=\"btn btn-primary mt-3\">Valider</button>
                    </div>

                    {{ form_end(updateRegistration) }}
                </div>
            </div>

            <br>
            <br>



        </div>
    </div>



{% endblock %}", "registration/registration-update.html.twig", "/home/clem/INFREP/introduction-symfo/templates/registration/registration-update.html.twig");
    }
}
